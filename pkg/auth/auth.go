package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

func DownloadCredentialsFile() error {
	cmd := exec.Command("bash", "-c", `curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash`)

	// Run the command
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("Error running script: %v\n", err)
	}

	fmt.Println("Secure files are downloaded")
	return nil
}

// SetupApplicationDefaultCredential composes the credential file used to authenticate Google API calls.
// The file name is composed by gitlabCIJobId; the file content is built on oidcJwt and Workload Identity Federation Provider provided by Gitlab CI.
// If serviceAccount is provided, the service Account impersonation is applied during authentication.
//
// See ADC details in: https://cloud.google.com/docs/authentication/application-default-credentials#GAC.
// See Gitlab Workload Identity Federation details in: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/.
func SetupApplicationDefaultCredential(gitlabCIJobId, oidcJwt, wip, serviceAccount string) error {
	// read jwt token input, write it to a JWT file
	jwtFile, err := createJWTFile(gitlabCIJobId, oidcJwt)
	if err != nil {
		return err
	}

	// compose the credential file, inlcuding JWT file
	credentialFile, err := createCredentialFile(gitlabCIJobId, jwtFile, wip, serviceAccount)
	if err != nil {
		return err
	}

	fmt.Printf("auth completed, file: %s", credentialFile)
	return nil
}

func createJWTFile(gitlabCIJobId, oidcJwt string) (string, error) {
	// Create file
	path := fmt.Sprintf("/tmp/oidc-jwt-%s.txt", gitlabCIJobId)
	file, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println("Error closing file:", err)
		}
	}()

	// Write data to file
	data := []byte(oidcJwt)
	_, err = file.Write(data)
	if err != nil {
		return "", err
	}

	return path, err
}

func createCredentialFile(ciId, jwtFilePath, wip, serviceAccount string) (string, error) {
	config := ExternalAccountConfig{
		Type:             "external_account",
		Audience:         wip,
		SubjectTokenType: "urn:ietf:params:oauth:token-type:jwt",
		TokenURL:         "https://sts.googleapis.com/v1/token",
		CredentialSource: CredentialSource{
			File: jwtFilePath,
			Format: Format{
				Type: "text",
			},
		},
	}
	if serviceAccount != "" {
		fmt.Printf("service account provided, authenticating use Workload Identity Federation with Service Account impersonation...")
		config.ServiceAccountImpersonationURL = fmt.Sprintf("https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/%s:generateAccessToken", serviceAccount)
	}

	// Convert the struct to JSON and write to file
	jsonBytes, err := json.Marshal(config)
	if err != nil {
		return "", nil
	}
	path := fmt.Sprintf("/tmp/oidc-credential-%s.json", ciId)
	err = ioutil.WriteFile(path, jsonBytes, 0644)
	if err != nil {
		return "", nil
	}

	return path, nil
}
