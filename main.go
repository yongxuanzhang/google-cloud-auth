/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab/quanzhang/google-cloud-auth/cmd"

func main() {
	cmd.Execute()
}
