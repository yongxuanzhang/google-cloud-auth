/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"gitlab/quanzhang/google-cloud-auth/pkg/auth"

	"github.com/spf13/cobra"
)

var (
	wip             string
	oidcJwt         string
	sa              string
	ciJobId         string
	credentialsFile string
)

// authCmd represents the auth command
var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Create the credential file for cloud client library authentication using WIF and ADC",
	Long: `Credential files is created at: /tmp/oidc-credential-{ci-job-id}.json.
	See ADC details in: https://cloud.google.com/docs/authentication/application-default-credentials#GAC.
	See WIF in https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/`,
	RunE: func(cmd *cobra.Command, args []string) error {
		// validation
		switch credentialsFile {
		case "":
			if wip == "" || oidcJwt == "" {
				return fmt.Errorf("input validation err: workload-identity-provider and gcp-oidc-jwt cannot be empty when credentials-file is not provided")
			}
		default:
			if wip != "" {
				return fmt.Errorf("input validation err: workload-identity-provider and credentials-file cannot coexist")
			}
		}

		switch credentialsFile {
		case "":
			fmt.Println("authenticating with Gitlab Workload Identity Provider...")
			if err := auth.SetupApplicationDefaultCredential(ciJobId, oidcJwt, wip, sa); err != nil {
				return err
			}
		default:
			fmt.Println("authenticating with user provided credential file...")
			if err := auth.DownloadCredentialsFile(); err != nil {
				return err
			}
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(authCmd)

	authCmd.PersistentFlags().StringVarP(&sa, "service-account", "s", "", "GCP Service Account")
	authCmd.PersistentFlags().StringVarP(&oidcJwt, "gcp-oidc-jwt", "o", "", "OIDC")
	authCmd.PersistentFlags().StringVarP(&ciJobId, "ci-job-id", "c", "", "Gitlab CI Job ID")
	authCmd.PersistentFlags().StringVarP(&wip, "workload-identity-provider", "w", "", "Workload Identity Federation")
	authCmd.PersistentFlags().StringVarP(&credentialsFile, "credentials-file", "", "", "Credentials File")
}
